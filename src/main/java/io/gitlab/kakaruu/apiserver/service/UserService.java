package io.gitlab.kakaruu.apiserver.service;

import io.gitlab.kakaruu.apiserver.model.LoginDTO;
import io.gitlab.kakaruu.apiserver.model.SignUpDTO;
import io.gitlab.kakaruu.apiserver.model.UserInfoDTO;

import java.security.NoSuchAlgorithmException;

public interface UserService {
  public UserInfoDTO signUp(SignUpDTO dto) throws NoSuchAlgorithmException;

  public UserInfoDTO login(LoginDTO dto) throws NoSuchAlgorithmException;

  public boolean Logout();

  public boolean withdraw();
}
