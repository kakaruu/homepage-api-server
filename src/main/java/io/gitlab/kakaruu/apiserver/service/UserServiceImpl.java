package io.gitlab.kakaruu.apiserver.service;

import io.gitlab.kakaruu.apiserver.model.LoginDTO;
import io.gitlab.kakaruu.apiserver.model.SignUpDTO;
import io.gitlab.kakaruu.apiserver.model.UserInfoDTO;
import io.gitlab.kakaruu.apiserver.repository.UserDAO;
import io.gitlab.kakaruu.apiserver.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Service
@Transactional
public class UserServiceImpl implements UserService {
  @Autowired
  UserDAO userDAO;

  @Override
  public UserInfoDTO signUp(SignUpDTO dto) throws NoSuchAlgorithmException {
    if(userDAO.insert(dto.getEmail(), new byte[0], dto.getName()) > 0) {
      int id = userDAO.selectLastInsertId();
      UserVO vo = userDAO.selectById(id);

      byte[] digest = genDigest(vo, dto.getPwd());
      userDAO.updatePrepareUser(id, digest);

      vo = userDAO.selectById(id);

      UserInfoDTO result = new UserInfoDTO();
      result.setId(vo.getId());
      result.setEmail(vo.getEmail());
      result.setName(vo.getName());
      result.setLevel(vo.getLevel());
      result.setRegDatetime(vo.getRegDatetime());
      result.setLastAccessDatetime(vo.getLastAccessDatetime());

      return result;
    }
    return null;
  }

  @Override
  public UserInfoDTO login(LoginDTO dto) throws NoSuchAlgorithmException {
    UserVO vo = userDAO.selectByEmail(dto.getEmail());
    if(vo != null) {
      byte[] digest = genDigest(vo, dto.getPwd());
      if(userDAO.findIdByEmailPwd(dto.getEmail(), digest) > 0) {
        UserInfoDTO result = new UserInfoDTO();
        result.setId(vo.getId());
        result.setEmail(vo.getEmail());
        result.setName(vo.getName());
        result.setLevel(vo.getLevel());
        result.setRegDatetime(vo.getRegDatetime());
        result.setLastAccessDatetime(vo.getLastAccessDatetime());
        result.setDenyDatetime(vo.getDenyDatetime());;

        return result;
      }
    }
    return null;
  }

  @Override
  public boolean Logout() {
    return false;
  }

  @Override
  public boolean withdraw() {
    return false;
  }

  private byte[] genDigest(UserVO vo, String pwd) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("SHA-512");
    MessageDigest md2 = MessageDigest.getInstance("SHA-512");

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    df.setTimeZone(TimeZone.getTimeZone("UTC"));
    String regDate = df.format(vo.getRegDatetime());

    String salt = String.format("%0128x", new BigInteger(1, md.digest(regDate.getBytes(StandardCharsets.UTF_8))));
    String saltedPwd = salt.substring(0, 64) + pwd;
    byte[] digest = md.digest(saltedPwd.getBytes(StandardCharsets.UTF_8));

    return digest;
  }
}
