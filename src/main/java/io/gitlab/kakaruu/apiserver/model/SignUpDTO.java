package io.gitlab.kakaruu.apiserver.model;

import lombok.Data;

@Data
public class SignUpDTO {
  private String email;
  private String pwd;
  private String name;
}
