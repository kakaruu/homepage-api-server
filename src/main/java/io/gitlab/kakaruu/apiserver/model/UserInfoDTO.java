package io.gitlab.kakaruu.apiserver.model;

import io.gitlab.kakaruu.apiserver.vo.UserVO;
import lombok.Data;

import java.util.Date;

@Data
public class UserInfoDTO {
  private int id;
  private String email;
  private String name;
  private UserVO.UserLevel level;
  private Date regDatetime;
  private Date lastAccessDatetime;
  private Date denyDatetime;
}
