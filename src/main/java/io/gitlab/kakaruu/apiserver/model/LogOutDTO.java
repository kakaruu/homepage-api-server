package io.gitlab.kakaruu.apiserver.model;

import lombok.Data;

@Data
public class LogOutDTO {
  String id;
}
