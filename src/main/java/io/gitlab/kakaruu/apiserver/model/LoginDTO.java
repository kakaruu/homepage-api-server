package io.gitlab.kakaruu.apiserver.model;

import lombok.Data;

@Data
public class LoginDTO {
  private String email;
  private String pwd;
}
