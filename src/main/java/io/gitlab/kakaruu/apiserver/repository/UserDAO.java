package io.gitlab.kakaruu.apiserver.repository;

import io.gitlab.kakaruu.apiserver.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDAO {
  public int findIdByEmailPwd(String email, byte[] pwd);

  public int updateAccessDateTime(int id);

  public int updatePrepareUser(int id, byte[] pwd);

  public UserVO selectById(int id);

  public UserVO selectByEmail(String email);

  public int insert(String email, byte[] pwd, String name);

  public int delete(int id);

  public int selectLastInsertId();
}
