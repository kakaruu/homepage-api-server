package io.gitlab.kakaruu.apiserver.controller;

import io.gitlab.kakaruu.apiserver.model.LoginDTO;
import io.gitlab.kakaruu.apiserver.model.SignUpDTO;
import io.gitlab.kakaruu.apiserver.model.UserInfoDTO;
import io.gitlab.kakaruu.apiserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.NoSuchAlgorithmException;

@Controller
public class UserController {
  @Autowired
  UserService userService;

  @ResponseBody
  @RequestMapping(value = "/signUp", method = RequestMethod.POST)
  public ResponseEntity<String> signUp(@RequestBody SignUpDTO body) throws NoSuchAlgorithmException {
    UserInfoDTO userInfo = userService.signUp(body);

    if(userInfo == null) {
      return new ResponseEntity<String>("잘못된 요청입니다.", HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<String>("가입 성공", HttpStatus.OK);
  }

  @ResponseBody
  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public ResponseEntity<String> login(@RequestBody LoginDTO body) throws NoSuchAlgorithmException {
    UserInfoDTO userInfo = userService.login(body);

    if(userInfo == null) {
      return new ResponseEntity<String>("잘못된 요청입니다.", HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<String>("로그인 성공", HttpStatus.OK);
  }
}
