package io.gitlab.kakaruu.apiserver.vo.typehandler;

public interface CodeEnum<T> {
  public T getCode();
}
