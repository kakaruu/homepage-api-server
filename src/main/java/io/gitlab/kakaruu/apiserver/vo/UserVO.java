package io.gitlab.kakaruu.apiserver.vo;

import io.gitlab.kakaruu.apiserver.vo.typehandler.CodeEnum;
import io.gitlab.kakaruu.apiserver.vo.typehandler.IntegerCodeEnumTypeHandler;
import lombok.Data;
import lombok.Getter;
import org.apache.ibatis.type.MappedTypes;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class UserVO {
  public enum UserLevel implements CodeEnum<Integer> {
    OWNER(10000000),
    ADMIN(9000000),
    WRITABLE_USER(2000),
    AUTHENTICATED_USER(1000),
    UNAUTHENTICATED_USER(100),
    PREPARE(0);

    private static final Map<Integer, UserLevel> intToEnum =
        Arrays.stream(values()).collect(Collectors.toMap(UserLevel::getCode, e -> e));

    @Getter
    private final Integer code;

    private UserLevel(int code) {
      this.code = code;
    }

    public static UserLevel fromCode(int code) {
      UserLevel level = intToEnum.get(code);
      return level;
    }

    @MappedTypes(UserLevel.class)
    public static class TypeHandler extends IntegerCodeEnumTypeHandler<UserLevel> {
    }
  }

  private int id;
  private String email;
  private String name;
  private UserLevel level;
  private Date regDatetime;
  private Date lastAccessDatetime;
  private Date denyDatetime;
}
