package io.gitlab.kakaruu.apiserver.vo.typehandler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeException;
import org.apache.ibatis.type.TypeHandler;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class IntegerCodeEnumTypeHandler<E extends CodeEnum<Integer>> implements TypeHandler<E> {
  @Override
  public void setParameter(PreparedStatement preparedStatement, int i, E parameter, JdbcType jdbcType) throws SQLException {
    preparedStatement.setInt(i, parameter.getCode());
  }

  @Override
  public E getResult(ResultSet resultSet, String columnName) throws SQLException {
    int code = resultSet.getInt(columnName);
    return getCodeEnum(code);
  }

  @Override
  public E getResult(ResultSet resultSet, int columnIndex) throws SQLException {
    int code = resultSet.getInt(columnIndex);
    return getCodeEnum(code);
  }

  @Override
  public E getResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
    int code = callableStatement.getInt(columnIndex);
    return getCodeEnum(code);
  }

  private E getCodeEnum(int code) {
    try {
      Type thisClass = getClass().getGenericSuperclass();
      Class<E> enumType = (Class<E>) ((ParameterizedType) thisClass).getActualTypeArguments()[0];

      E[] enumConstants = enumType.getEnumConstants();
      for (E codeEnum : enumConstants) {
        if (codeEnum.getCode() == code) {
          return codeEnum;
        }
      }
      return null;
    } catch (Exception e) {
      throw new TypeException("Can't make enum object", e);
    }
  }
}
